package com.mitocode;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.mitocode.model.Cliente;
import com.mitocode.model.Menu;
import com.mitocode.model.Rol;
import com.mitocode.model.Usuario;
import com.mitocode.service.IMenuService;
import com.mitocode.service.IRolService;
import com.mitocode.service.IUsuarioService;

@SpringBootTest
class CineappBackendApplicationTests {

	@Autowired
	private IUsuarioService service;

	@Autowired
	private BCryptPasswordEncoder bcrypt;
	
	@Autowired
	private IRolService rolService;
	
	@Autowired
	private IMenuService menuService;

	@Test
	void contextLoads() {
	}

	/*@Test
	public void crearUsuario() {
		Usuario us = new Usuario();
		us.setIdUsuario(3);
		us.setNombre("mitocodes");
		us.setClave(bcrypt.encode("123"));
		us.setEstado(true);

		Cliente c = new Cliente();
		c.setIdCliente(3);
		c.setNombres("MITOCODES");
		c.setApellidos("DELGADO");
		c.setDni("72301306");
		c.setFechaNac(LocalDate.of(1991, 1, 21));
		c.setUsuario(us);
		us.setCliente(c);

		Usuario retorno = service.registrarTransaccional(us);

		assertTrue(retorno.getClave().equalsIgnoreCase(us.getClave()));
	}
	*/
	/*
	@Test
	public void crearRol() {
		Rol rol = new Rol();
		rol.setDescripcion("DESCP");
		rol.setNombre("ROL P");
		Rol rols = rolService.registrar(rol);
		
		
	}
	*/
	@Test
	public void crearMenu() {
		Menu menu = new Menu();
		//menu.setIdMenu(11);
		menu.setIcono("ppt");
		menu.setNombre("asd");
		menu.setUrl("URL");
		
		//menu.setRoles(new ArrayList<>());
		Menu menus = menuService.registrar(menu);
		//assertTrue(menus.getIdMenu().equals(menu.getIdMenu()));
		
	}
	

}
